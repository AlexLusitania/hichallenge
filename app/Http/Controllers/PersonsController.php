<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PersonsController extends Controller {
  public function index() {
    return response(Person::all()->jsonSerialize(), Response::HTTP_OK);
  }
}