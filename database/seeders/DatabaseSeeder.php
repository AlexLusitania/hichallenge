<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            'name' => 'Declan Rice',
            'alias' => 'declan'
        ]);

        DB::table('persons')->insert([
            'name' => 'Sébastien Haller',
            'alias' => 'sebhaller'
        ]);

        DB::table('persons')->insert([
            'name' => 'Issa Diop',
            'alias' => 'diop3'
        ]);

        DB::table('persons')->insert([
            'name' => 'Felipe Anderson',
            'alias' => 'fanderson8'
        ]);

        DB::table('persons')->insert([
            'name' => 'Jarrod Bowen',
            'alias' => 'jarrod'
        ]);

        DB::table('persons')->insert([
            'name' => 'Tomáš Souček',
            'alias' => 'soucektomas'
        ]);

        DB::table('persons')->insert([
            'name' => 'Manuel Lanzini',
            'alias' => 'manulanzini'
        ]);

        DB::table('persons')->insert([
            'name' => 'Andriy Yarmolenko',
            'alias' => 'yarmo7'
        ]);

        DB::table('persons')->insert([
            'name' => 'Michail Antonio',
            'alias' => 'antwest'
        ]);

        DB::table('persons')->insert([
            'name' => 'Fabián Balbuena',
            'alias' => 'balbuena'
        ]);

        DB::table('persons')->insert([
            'name' => 'Angelo Ogbonna',
            'alias' => 'ogbelo'
        ]);

        DB::table('persons')->insert([
            'name' => 'Aaron Cresswell',
            'alias' => 'cresswell'
        ]);

        DB::table('persons')->insert([
            'name' => 'Robert Snodgrass',
            'alias' => 'snodgrassrob'
        ]);

        DB::table('persons')->insert([
            'name' => 'Arthur Masuaku',
            'alias' => 'masuaku'
        ]);

        DB::table('persons')->insert([
            'name' => 'Mark Noble',
            'alias' => 'captainoble'
        ]);
    }
}
